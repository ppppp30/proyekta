﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {

	private bool walking = false;
	public float speed = 0.9f;
	private Vector3 SpawnPoint;

	// Use this for initialization
	void Start () {
		SpawnPoint = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (walking) {
			transform.position = transform.position + Camera.main.transform.forward * speed * Time.deltaTime;
		}
		if (transform.position.y < -10f) {
			transform.position = SpawnPoint;
		}

		Ray ray = Camera.main.ViewportPointToRay (new Vector3 (0.5f, 0.5f, 0));
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit)) {
			if (hit.collider.name.Contains ("Floor")) {
				walking = true;
			} else {
				walking = false;
			}
		}
			
	}
}
